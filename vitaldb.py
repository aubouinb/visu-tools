"""Create a simple stocks correlation dashboard.

Choose stocks to compare in the drop down widgets, and make selections
on the plots to update the summary and histograms accordingly.

"""


# -------- import package data ----------
import os
from datetime import datetime
import pandas as pd
import numpy as np
from bokeh.io import curdoc
from bokeh.layouts import column, row
from bokeh.models import ColumnDataSource, TextInput, Slider, Button, HoverTool
from bokeh.plotting import figure
from bokeh.models import Div, Spinner, MultiChoice, CheckboxGroup, BoxAnnotation, CustomJS, Select, LinearAxis, Range1d
from bokeh.models.widgets import TableColumn, DataTable
import bokeh
from get_vital_data import Full_import_local, print_info, formate_patient_data

# -------- Init data ----------

df = pd.DataFrame()
label_df = pd.DataFrame(columns=["Time_start", "Time_end", "Label"])
perso_data = pd.read_csv("./info_clinic_vitalDB.csv", index_col=0, decimal='.')
perso_data = formate_patient_data(perso_data)
flag = False
available_file = os.listdir('../Vitaldb_database_2/')
available_case = [int(name[:-4]) for name in available_file if name[-4:] == ".csv"]
available_case.sort()
available_case = [str(el) for el in available_case]

# ------------- Widget deffition ------------
# input for patient id
select_id = Select(title="Patient id:", width=100,
                   options=available_case, value=available_case[0])
# chechbox to load only the basical signals
filtre = CheckboxGroup(labels=['Filter'], active=[], width_policy="min")
# Button to load the data into the server
Load_Button = Button(label="Load", width_policy="min")
# Button to access to the documentation
Doc_Button = Button(label="Data documentation", width_policy="min")
# Spinner to choose the number of figure on the web page
fig_number = Spinner(title="Number of figures:", low=1,
                     high=10, step=1, value=3, visible=False)

# text for the zoom option
t_zoom = Div(text="""<p>Zoom:</p>""", width=50, height=30, visible=False)
# slider to define start and end of the the zoomed windows
Zoom_start = Slider(title="Start", value=80, start=10,
                    end=300, step=1, visible=False)
Zoom_end = Slider(title="End", value=90, start=10,
                  end=300, step=1, visible=False)
Zoom_move = Slider(title="Move window", value=0,
                   start=0, end=1, step=1, visible=False)
# text which will include the patient information
info_patient = Div(text="""  """, visible=True, width=300)

# text to introduce the Labelling panel
t_Label = Div(text="""<p> <strong> Labelling: </strong> </p>""",
              width=50, height=30, visible=False)
# Dropdown to choose the practitionner
d_Practi = Select(title="Practitionner:", options=[
                  'Remi', 'Benjamin', 'Non-expert'], value='Non-expert', visible=False)
# input for the annotation
Time_start = TextInput(title="Time_start (s)",
                       width=100, value='', visible=False)
Time_end = TextInput(title="Time_end (s)", width=100, value='', visible=False)
Label_input = TextInput(title="Label", width=100, value='', visible=False)
# add annotation button
Add_Button = Button(label="Create", width_policy="min", visible=False)

# table of all labels
source = ColumnDataSource(label_df)
table_columns = [TableColumn(field=name, title=name)
                 for name in label_df.columns]
data_table = DataTable(source=source, columns=table_columns, editable=True,
                       visible=False, width=300, height=200)  # , width_policy = "min"

# Save and load alabeks buttons
Save_button = Button(label="Save labels", width_policy="min", visible=False)
load_label_button = Button(label="Load label file",
                           width_policy="min", visible=False)
# load auto labelling with hard treshold in it
load_autolabel_button = Button(
    label="Load auto label file", width_policy="min", visible=False)
slider_auto = TextInput(title="Treshold:", value='0', visible=False)

# special figure
special_figure = figure(width=1100, height=300,
                        title="special figure", visible=False)
select_special = Select(title="signal:", options=[], visible=False)
special_treshold = Spinner(title="Treshold (%):", low=5,
                           high=100, step=5, value=10,
                           visible=False, width=100)
# layout creation
Label_layout = column(t_Label, d_Practi, row(Time_start, Time_end),
                      row(Label_input, Add_Button), data_table,
                      row(Save_button, load_label_button,
                          load_autolabel_button), slider_auto)


option = column(row(select_id, column(filtre, Load_Button), Doc_Button),
                fig_number, column(t_zoom, Zoom_start, Zoom_end, Zoom_move),
                info_patient, Label_layout)


layout = row(option, column(
    row(special_figure, select_special, special_treshold)))


# ----------- API functions ------------------

def change_click():
    """Load data from vitalDB and display all the widget on the page."""
    global df, patient_id, label_df
    print('loading...')
    try:
        patient_id = int(select_id.value)
    except:
        print("Patient id must be an integer")
        return
    df = Full_import_local(patient_id, filtered=len(filtre.active))
    df = df.ffill()
    df.fillna(value=0, inplace=True)
    patient = perso_data.iloc[[patient_id-1]]
    info_patient.text = print_info(patient, patient_id)

    fig_number.visible = True
    t_zoom.visible = True
    Zoom_start.visible = True
    Zoom_end.visible = True
    Zoom_move.visible = True
    t_Label.visible = True
    d_Practi.visible = True
    Time_start.visible = True
    Time_end.visible = True
    Label_input.visible = True
    Add_Button.visible = True
    data_table.visible = True
    Save_button.visible = True
    special_figure.visible = True
    select_special.visible = True
    special_treshold.visible = True

    select_special.options = [col for col in df.columns]
    select_special.value = 'Solar8000/ART_MBP'

    update_figure_number(fig_number.value, 0, fig_number.value)
    # update_special_fig(0, 0, 0)

    Zoom_start.start = 0
    Zoom_start.end = len(df)
    Zoom_start.value = 0
    Zoom_end.start = 0
    Zoom_end.end = len(df)
    Zoom_end.value = len(df)
    Zoom_move.start = - len(df)
    Zoom_move.end = len(df)

    file_list = os.listdir('./data/' + d_Practi.value + '/')
    file_list.sort(reverse=True)
    name = 'Label_Patient_' + str(patient_id) + '_202'
    name_auto = 'Label_Patient_' + str(patient_id) + '_Auto'
    load_label_button.visible = False
    load_autolabel_button.visible = False
    slider_auto.visible = False
    for file in file_list:
        if name in file:
            load_label_button.visible = True
        if name_auto in file:
            load_autolabel_button.visible = True
    label_df = pd.DataFrame(columns=['Time_start', 'Time_end', 'Label'])
    maj_label_plot()
    # update_special_fig(0, 0, 0)
    print('loading ok')


def update_special_fig(attr, old, new):
    """Update the first figure, with label for extrem value."""
    global red_box_visible
    if len(special_figure.renderers) != 0:
        special_figure.renderers = []
        special_figure.legend.items = []

    try:
        for b in red_box_visible:
            b.visible = False
        red_box_visible = []
    except:
        red_box_visible = []

    tracks = df[select_special.value]
    median = np.median(tracks)
    tracks = (tracks-median)/median * 100
    color = list(bokeh.palettes.brewer['Dark2'][3])
    special_figure.line(np.arange(0, len(df)), tracks,
                        legend_label=select_special.value, color=color[0])

    al = 0
    for i in range(len(tracks)):
        if abs(tracks[i]) > special_treshold.value:
            if al == 0:
                start = i
                al = 1
        else:
            if al == 1:
                end = i
                al = 0
                if end-start > 10:
                    red_box = BoxAnnotation(
                        left=start, right=end, fill_color='#D6604D', fill_alpha=0.1)
                    red_box_visible.append(red_box)
                    special_figure.add_layout(red_box)


def actualize_plot(attr, old, new):
    """Actualize plot with the new selected signals."""
    global fig_list, selctor_list
    for i in range(len(fig_list)):
        if new == selctor_list[i].value:
            fig = fig_list[i]
            break
    if len(fig.renderers) != 0:
        fig.renderers = []
        fig.legend.items = []

    i = 0
    tracks = new
    color = list(bokeh.palettes.brewer['Dark2'][max(3, len(tracks)+3)])
    max_T = max([max(df[name]) for name in tracks])
    coeff = np.ones(len(tracks))
    for name in tracks:
        if (name == 'Solar8000/PLETH_HR' or name == 'Solar8000/HR') and len(fig.extra_y_ranges) == 1:
            fig.line(np.arange(0, len(df)), df[name], legend_label=name + '(left scale)',
                     color=color[i], y_range_name="foo")
            tooltips = [('Time', "$x{n}"), ("Value", "$y")]
            fig.add_tools(HoverTool(renderers=[plot], tooltips=tooltips))
        else:
            max_t = max(df[name])
            coeff[i] = int(max(0.1, round(max_T/10/max_t))*10)
            source = ColumnDataSource(data=dict(x=np.arange(0, len(df)), y=df[name]*coeff[i], true_value=df[name]))
            plot = fig.line('x', 'y', source=source, legend_label=name + '( x' + str(coeff[i]) + ')', color=color[i])
            tooltips = [('Time', "$x{n}"), ("Value", "$true_value{0.00}")]
            fig.add_tools(HoverTool(renderers=[plot], tooltips=tooltips))
        i += 1
        if 'pred_df' in globals():
            if name == 'BIS/BIS':
                fig.circle(pred_df['Time'], pred_df['BIS_pred'],
                           legend_label='BIS_pred', color=color[i], radius=0.4)
                i += 1
            elif name == 'Solar8000/ART_MBP':
                fig.circle(pred_df['Time'], pred_df['MAP_pred'],
                           legend_label='MAP_pred', color=color[i], radius=0.4)
                i += 1
            elif name == 'Solar8000/PLETH_HR':
                fig.circle(pred_df['Time'], pred_df['HR_pred'],
                           legend_label='HR_pred', color=color[i], radius=0.4)
                i += 1


def create_fig(i):
    """Create the i-th figure."""
    global fig
    fig = figure(width=1100, height=300)
    names = [col for col in df.columns]
    if i == 0:
        if len(filtre.active) == 0:
            name_list = ['BIS/BIS', 'Primus/ETCO2', 'Primus/PIP_MBAR', 'Primus/TV']
        else:
            name_list = ['BIS/BIS']
    elif i == 1:
        name_list = ['Solar8000/ART_MBP', 'Solar8000/HR',
                     'Solar8000/PLETH_HR', 'Solar8000/PLETH_SPO2']
        fig.extra_y_ranges = {"foo": Range1d(start=50, end=150)}
        fig.add_layout(LinearAxis(y_range_name="foo"), 'right')
    elif i == 2:
        name_list = ['Orchestra/RFTN20_RATE', 'Orchestra/RFTN20_CE',
                     'Orchestra/PPF20_RATE', 'Orchestra/PPF20_CE']
    else:
        name_list = [names[i]]
    select_fig = MultiChoice(title="Track to plot:",
                             value=name_list, options=names)

    return fig, select_fig


def update_figure_number(attr, old, new):
    """Update the number of figure on the web page."""
    global layout, fig_list, selctor_list, col, flag
    if flag:
        layout.children[1].children.remove(col)
        Zoom_start.value = 0
        Zoom_end.value = len(df)
    fig_list = []
    selctor_list = []
    for i in range(fig_number.value):
        fig, slct = create_fig(i)
        fig_list.append(fig)
        selctor_list.append(slct)
        actualize_plot(selctor_list[i].value, [], selctor_list[i].value)
        selctor_list[i].on_change("value", actualize_plot)
        if i == 0:
            col = row(fig, slct)
        else:
            col = column(col, row(fig, slct))
    layout.children[1].children.append(col)
    flag = True


def zoom_in_act(attr, old, new):
    """Actualize zoom."""
    global fig_list
    zoom_start = Zoom_start.value
    zoom_end = Zoom_end.value
    special_figure.x_range.start = zoom_start
    special_figure.x_range.end = zoom_end
    for fig in fig_list:
        fig.x_range.start = zoom_start
        if len(fig.extra_y_ranges) == 1:
            fig.x_range.end = zoom_end-(zoom_end-zoom_start)*2.9/100
        else:
            fig.x_range.end = zoom_end


def zoom_move_apply(attr, old, new):
    """Actualize zoom for zoom."""
    Zoom_start.value += new-old
    Zoom_end.value += new-old


def display_annotation():
    """Display the loaded label file on the tbale and on the figures."""
    global fig_list, source
    label_df.loc[len(label_df.index)] = [int(Time_start.value),
                                         int(Time_end.value),
                                         Label_input.value]
    source.data = {"Time_start":    label_df.Time_start,
                   "Time_end":      label_df.Time_end,
                   "Label":         label_df.Label}
    Time_start.value = ''
    Time_end.value = ''
    Label_input.value = ''
    for i in label_df.index:
        green_box = BoxAnnotation(
            left=label_df["Time_start"][i], right=label_df["Time_end"][i],
            fill_color='#009E73', fill_alpha=0.2)
        for fig in fig_list:
            fig.add_layout(green_box)


def save():
    """Save the current label into a file."""
    now = datetime.now()
    name = "Label_Patient_" + str(patient_id) + \
        "_" + now.strftime("%Y_%m_%d_%H_%M") + '.csv'
    label_df.to_csv('./data/' + d_Practi.value + '/' + name)


def maj_label_plot():
    """Actualize label plot on the figures."""
    global source, label_df, fig_list, box_list
    source.data = {"Time_start":    label_df.Time_start,
                   "Time_end":      label_df.Time_end,
                   "Label":         label_df.Label}
    for fig in fig_list:
        fig.renderers = [r for r in fig.renderers if r.name != 'Label']
    try:
        for b in box_list:
            b.visible = False
        box_list = []
    except:
        box_list = []
    for i in label_df.index:
        green_box = BoxAnnotation(
            name='Label', left=label_df["Time_start"][i],
            right=label_df["Time_end"][i],
            fill_color='#009E73', fill_alpha=0.2)
        box_list.append(green_box)
        print(label_df["Time_end"][i])
        for fig in fig_list:
            fig.add_layout(green_box)


def load_file():
    """Load last label file created."""
    global label_df, fig_list, source
    print('loading...')
    file_list = os.listdir('./data/' + d_Practi.value + '/')
    file_list.sort(reverse=True)
    name = 'Label_Patient_' + str(patient_id) + '_'
    flag = False
    for file in file_list:
        if name in file:
            label_df = pd.read_csv(
                "./data/" + d_Practi.value + '/' +
                file, index_col=0, decimal='.')
            flag = True
            break
    if not flag:
        print("There is no label file for this patient")
        return
    Time_start.value = ''
    Time_end.value = ''
    Label_input.value = ''
    maj_label_plot()
    print('load ok')


def load_auto_file():
    """Load data from automatic labelling file."""
    global fig_list, source, alarm_df, label_df, pred_df
    file_list = os.listdir('./data/Auto')
    file_list.sort(reverse=True)
    name = 'Label_Patient_' + str(patient_id) + '_Auto_'
    name_pred = 'Pred_' + str(patient_id)
    flag = False
    for file in file_list:
        if name in file:
            alarm_df = pd.read_csv("./data/" + file, index_col=0, decimal='.')
            slider_auto.visible = True
            flag = True
            break
    for file in file_list:
        if name_pred in file:
            pred_df = pd.read_csv("./data/" + file, index_col=0, decimal='.')
            print('Pred file loaded')
            break
    if not flag:
        print("There is no label file for this patient")
        return

    label_df = pd.DataFrame(columns=['Time_start', 'Time_end', 'Label'])

    al = 0

    slider_auto.value = str(3*np.std(alarm_df['norm_alarm']))
    for i in alarm_df.index:
        if alarm_df['norm_alarm'][i] >= float(slider_auto.value):
            if al == 0:
                start = alarm_df['Time'][i]
                al = 1
        else:
            if al == 1:
                end = alarm_df['Time'][i]
                al = 0
                label_df = label_df.append(
                    dict(zip(label_df.columns, [start, end, 'Auto'])), ignore_index=True)
    maj_label_plot()


def treshold_change(attr, old, new):
    """Change the treshold to generate automatic labels for the current case."""
    global alarm_df, label_df
    label_df = pd.DataFrame(columns=['Time_start', 'Time_end', 'Label'])
    al = 0
    for i in alarm_df.index:
        if alarm_df['norm_alarm'][i] >= float(slider_auto.value):
            if al == 0:
                start = alarm_df['Time'][i]
                al = 1
        else:
            if al == 1:
                end = alarm_df['Time'][i]
                al = 0
                label_df = label_df.append(
                    dict(zip(label_df.columns, [start, end, 'Auto'])), ignore_index=True)
    maj_label_plot()


def actualise_load_option(attr, old, new):
    """Actualize the visibility of the load button wether a label file is available or not."""
    file_list = os.listdir('./data/' + d_Practi.value + '/')
    file_list.sort(reverse=True)
    name = 'Label_Patient_' + str(patient_id) + '_202'
    load_label_button.visible = False
    for file in file_list:
        if name in file:
            load_label_button.visible = True


def update_label(attr, old, new):
    """Update the label dataframe after a manual change in the table."""
    global label_df, source
    label_df = pd.DataFrame(source.data)
    maj_label_plot()
    print(label_df.head())
    print("update label file")


# -------- Callback definition ----------
Doc_Button.js_on_click(CustomJS(args=dict(
    url="https://vitaldb.net/docs/?documentId=13qqajnNZzkN7NZ9aXnaQ-47NWy7kx-a6gbrcEsi-gak"),
    code="window.open(url)"))

Load_Button.on_click(change_click)
fig_number.on_change("value", update_figure_number)
Zoom_start.on_change("value", zoom_in_act)
Zoom_end.on_change("value", zoom_in_act)
Zoom_move.on_change("value", zoom_move_apply)
select_special.on_change("value", update_special_fig)
special_treshold.on_change("value", update_special_fig)

slider_auto.on_change("value", treshold_change)
Add_Button.on_click(display_annotation)
Save_button.on_click(save)
load_label_button.on_click(load_file)
load_autolabel_button.on_click(load_auto_file)
d_Practi.on_change("value", actualise_load_option)
# source.on_change('value', update_label)
# start server
curdoc().add_root(layout)
curdoc().title = "VitalDB visu"

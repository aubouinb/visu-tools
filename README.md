# Visu Tools

Interface to visualize vital signal during anesthesia from the  [vitalDB database](https://vitaldb.net/).



# Usage

- Download all the folder on your computer and use terminal command:

```
bokeh serve --enable-xsrf-cookies --auth-module=auth.py  Bokeh-interface.py
```

This open the GUI in your browser. 
- Then you have to choose a patient identifier (an integer between 1 and 6550), check or uncheck the box to download all the recorded track or only the esssential ones (BIS, MAP, HR, ETCO2, SPO2, Propofol & Remifentani rate and Ce) and press the _Load_ button.



## License

_GNU General Public License 3.0_

## Project status
First working version, waiting for review to improve the design.

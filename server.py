from bokeh.server.auth_provider import AuthModule
from bokeh.command.util import build_single_handler_application
from bokeh.server.server import Server

auth_provider = AuthModule('./auth.py')  # from bokeh.server.auth_provider import AuthModule
# from bokeh.command.util import build_single_handler_application
app = build_single_handler_application('./vitaldb.py')


AUTH_ENDPOINT = 'login'
prefix = 'vitaldb'
app_kwargs = {
    'port': 5011,
    'prefix': prefix,
    'auth_provider': auth_provider,
}

app = Server({'/': app}, **app_kwargs)  # from bokeh.server.server import Server

# Due to a bug in Bokeh, we must add the prefix to auth_provider._module
if isinstance(auth_provider, AuthModule) and not auth_provider._module.login_url.startswith(prefix):
    auth_provider._module.login_url = f'/{prefix}/{AUTH_ENDPOINT}'

app.start()
app.io_loop.add_callback(app.show, "/")
app.io_loop.start()

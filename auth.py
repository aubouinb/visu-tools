#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 27 16:06:52 2022

@author: aubouinb
"""
import tornado
from tornado.web import RequestHandler


# could define get_user_async instead
users_list = {'Bob': 'damon', 'Mirko': 'damon', 'Thao': 'damon', 'Remi': 'damon', 'Benjamin': 'damon'}


def get_user(request_handler):
    try:
        user = request_handler.get_cookie("user")
        user = user.replace('"', '')
        if user in users_list:
            return (user)
        else:
            return None
    except:
        return None


# could also define get_login_url function (but must give up LoginHandler)
login_url = "/login"
html_file = "./vitaldb-login.html"

# optional login page for login_url


class LoginHandler(RequestHandler):

    def get(self):
        try:
            errormessage = self.get_argument("error")
        except Exception:
            errormessage = ""
        # change the path of the action in the html file

        with open(html_file, "r") as file:
            html_content = file.read()

        # Replace the action attribute dynamically
        new_html_content = html_content.replace('action="/login"', f'action="{login_url}"')

        with open(html_file, "w") as file:
            file.write(new_html_content)

        self.render(html_file, errormessage=errormessage)

    def check_permission(self, username, password):
        if username in users_list and users_list[username] == password:
            return True
        return False

    def post(self):
        username = self.get_argument("username", "")
        password = self.get_argument("password", "")
        auth = self.check_permission(username, password)

        if auth:
            self.set_current_user(username)
            self.redirect(login_url.replace('/login', ''))
        else:
            error_msg = "?error=" + tornado.escape.url_escape("Login incorrect")
            self.redirect(login_url + error_msg)

    def set_current_user(self, user):
        if user:
            self.set_cookie("user", tornado.escape.json_encode(user))
        else:
            self.clear_cookie("user")


# optional logout_url, available as curdoc().session_context.logout_url
logout_url = "/vitaldb-logout"

# optional logout handler for logout_url


class LogoutHandler(RequestHandler):

    def get(self):
        self.clear_cookie("user")
        self.redirect(self.get_argument("next", "/"))

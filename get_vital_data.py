#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  5 15:50:47 2022

@author: aubouinb
"""

import pandas as pd
import numpy as np


def Full_import_local(patient_id, Te=1, filtered=False):
    tracks = pd.read_csv("../Vitaldb_database_2/" + str(patient_id) + ".csv")
    if filtered:
        col = ['BIS/BIS', 'Solar8000/PLETH_HR', 'Solar8000/ART_MBP', 'Orchestra/PPF20_RATE', 'Orchestra/RFTN20_RATE',
               'Orchestra/PPF20_CE', 'Orchestra/RFTN20_CE', 'Solar8000/ETCO2', 'Solar8000/PLETH_SPO2']
        L = [col[i] in tracks.columns for i in range(len(col))]
        if min(L):
            tracks = tracks[col]
        else:
            print("filter impossible")
    return tracks


def print_info(patient, caseid):
    '''Print the important information on the given operation'''
    patient = patient.copy()
    patient.fillna(value=-1, inplace=True)
    text = ''
    text += "caseid: {}, age: {}, height: {}, weight: {}, sex: {}, ASA score: {}".format(caseid, int(patient['age'].iloc[0]), int(
        patient['height'].iloc[0]), int(patient['weight'].iloc[0]), int((patient['sex'].iloc[0] == 'M')), int(patient['asa'].iloc[0]))
    text += '<br>'
    text += "Operation name: {}".format(patient['opname'].iloc[0])
    text += '<br>'
    text += "Operation type: {}".format(patient['optype'].iloc[0])
    text += '<br>'
    text += "Diagnostic: {}".format(patient['dx'].iloc[0])
    text += '<br>'
    text += "Anesthesie type: {}, Emergency ? {} ".format(
        patient['ane_type'].iloc[0], patient['emop'].iloc[0])
    text += '<br>'
    text += "Blood loss: {} mL".format(int(patient['intraop_ebl'].iloc[0])) + \
        ",  Intraoperative crystalloid: {} mL".format(int(patient['intraop_crystalloid'].iloc[0]))
    text += '<br>'
    text += "Intraoperative RBC transfusion: {} mL".format(
        int(patient['intraop_ebl'].iloc[0])) + ",  Intraoperative FFP transfusion: {} mL".format(int(patient['intraop_ffp'].iloc[0]))
    text += '<br>'
    text += "Used drugs in Bolus:"
    text += '<br>'
    if int(patient["intraop_ppf"].iloc[0]) > 0:
        text += "Propofol : {} mg".format(int(patient["intraop_ppf"].iloc[0]))
        text += '<br>'
    if int(patient["intraop_mdz"].iloc[0]) > 0:
        text += "Midazolam : {} mg".format(int(patient["intraop_mdz"].iloc[0]))
        text += '<br>'
    if int(patient["intraop_ftn"].iloc[0]) > 0:
        text += "Fentanyl : {} mcg".format(int(patient["intraop_ftn"].iloc[0]))
        text += '<br>'
    if int(patient["intraop_rocu"].iloc[0]) > 0:
        text += "Rocuronium : {} mg".format(int(patient["intraop_rocu"].iloc[0]))
        text += '<br>'
    if int(patient["intraop_vecu"].iloc[0]) > 0:
        text += "Vecuronium : {} mg".format(int(patient["intraop_vecu"].iloc[0]))
        text += '<br>'
    if int(patient["intraop_eph"].iloc[0]) > 0:
        text += "Ephedrine : {} mg".format(int(patient["intraop_eph"].iloc[0]))
        text += '<br>'
    if int(patient["intraop_phe"].iloc[0]) > 0:
        text += "Phenylephrine : {} mcg".format(int(patient["intraop_phe"].iloc[0]))
        text += '<br>'
    if int(patient["intraop_epi"].iloc[0]) > 0:
        text += "Epinephrine : {} mcg".format(int(patient["intraop_epi"].iloc[0]))
        text += '<br>'
    if int(patient["intraop_ca"].iloc[0]) > 0:
        text += "Calcium chloride : {} mg".format(int(patient["intraop_ca"].iloc[0]))
        text += '<br>'
    return text


def formate_patient_data(data):
    '''format the data extracted from the .csv file to be able to read them as integers'''
    for s in ['age', 'height', 'weight']:
        if s == 'age':
            data[s] = data[s].str.replace(r'.', '0', regex=False)
        else:
            data[s] = data[s].str.replace(r'. ', '0', regex=False)
        data[s] = data[s].str.replace(r' %', '0', regex=False)
        data[s] = data[s].str.replace(r'0%', '0', regex=False)
        data[s] = data[s].astype(float)
    return data


def fill_Nan(L):
    '''Replace Nan by last non Nan value'''
    nans = np.isnan(L)
    c = 0
    for c in range(len(nans)):
        if nans[c] and c > 0:
            L[c] = L[c-1]
        elif nans[c] and c == 0:
            L[c] = 0
    return L
